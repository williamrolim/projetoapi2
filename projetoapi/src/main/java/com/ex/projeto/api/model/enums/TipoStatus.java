package com.ex.projeto.api.model.enums;

public enum TipoStatus {
	ATIVO("ativo"), 
	INATIVO("inativo");
	
	private final String tipoStatus;
	
	private TipoStatus (String tipoStatus) {
		this.tipoStatus = tipoStatus;
	}
	
	public String getDescricao() {
		return tipoStatus;
	}
	
}
