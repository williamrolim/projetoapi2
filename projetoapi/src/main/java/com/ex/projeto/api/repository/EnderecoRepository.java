package com.ex.projeto.api.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.ex.projeto.api.model.Cliente;
import com.ex.projeto.api.model.Endereco;
import com.ex.projeto.api.model.enums.TipoStatus;

@Repository
public interface EnderecoRepository extends JpaRepository<Endereco, Long>{
	@Query("SELECT COUNT(e) FROM Endereco e Where e.cliente.id=?1 and e.status=?2")
	Integer countClienteIdAndStatus(Long id, TipoStatus status);

	Optional<Endereco> findByIdAndCliente(Long idEndereco, Cliente cliente);
	
}
