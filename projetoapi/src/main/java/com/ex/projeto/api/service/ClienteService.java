package com.ex.projeto.api.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

//import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ex.projeto.api.model.Cliente;
import com.ex.projeto.api.model.Documento;
import com.ex.projeto.api.model.Endereco;
import com.ex.projeto.api.model.enums.TipoStatus;
import com.ex.projeto.api.repository.ClienteRepository;
import com.ex.projeto.api.repository.EnderecoRepository;
@Service
public class ClienteService {

	@Autowired
	private ClienteRepository clienteRepository;
	@Autowired
	private EnderecoRepository enderecoRepository;
//	@Autowired
//	private DocumentoRepository documentoRepository;

	@Transactional
	public Cliente saved(Cliente cliente) {
		Cliente clientes = clienteRepository.save(cliente);
		return clientes;
	}

	@Transactional
	public Page<Cliente> getAll(Pageable pageable) {
		Page<Cliente> findAll = clienteRepository.findAll(pageable);
		return findAll;
	}

	@Transactional
	public Cliente getById(Long id) {
		Optional<Cliente> findbyId = clienteRepository.findById(id);
		findbyId.orElseThrow(() -> new RuntimeException("Cliente não encontrado"));
		return findbyId.get();
	}
	
	@Transactional
	public Cliente createAddresInClient(Endereco endereco, Long id) {
		List<Endereco> listEndereco = new ArrayList<>();
        Cliente clientePersistido;

		Optional<Cliente> clientes = clienteRepository.findById(id);//pegamos o cliente id, pelo que ele está no momento
		clientes.orElseThrow(() -> new RuntimeException("Client não encontrado"));//caso não achar o cliente
		
		Integer adressAtive = enderecoRepository.countClienteIdAndStatus(id, TipoStatus.ATIVO);
		Integer adressInative = enderecoRepository.countClienteIdAndStatus(id, TipoStatus.INATIVO);

		if(adressAtive >= 1) {
			throw new RuntimeException("ERROR: Não pode existir mais que um endereco ativo");
		}else if (adressInative >- 1) {
	
		endereco.setCliente(clientes.get());
		listEndereco.add(endereco);
		
		clientes.get().setEnderecos(listEndereco);
		}
		clientePersistido  = clienteRepository.save(clientes.get());
		return clientePersistido;

	}

	@Transactional
	public Cliente createDocumentInClient(Documento documentos, Long id) {
		List<Documento> listDocumento = new ArrayList<>();
        Cliente clientePersistido;

		Optional<Cliente> clientes = clienteRepository.findById(id);//pegamos o cliente id, pelo que ele está no momento
		clientes.orElseThrow(() -> new RuntimeException("Client não encontrado"));//caso não achar o cliente

	
		documentos.setCliente(clientes.get());
		listDocumento.add(documentos);
		
		clientes.get().setDocumentos(listDocumento);
	
		clientePersistido  = clienteRepository.save(clientes.get());
		return clientePersistido;		
	}
	
	

}
