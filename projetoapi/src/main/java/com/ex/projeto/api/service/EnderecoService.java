package com.ex.projeto.api.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ex.projeto.api.model.Cliente;
import com.ex.projeto.api.model.Endereco;
import com.ex.projeto.api.repository.EnderecoRepository;

@Service
public class EnderecoService {
	
	@Autowired
	EnderecoRepository enderecoRepository;
	
	@Autowired
	ClienteService clienteService;
	
	  
	@Transactional
	public Endereco getAllClientsAndAdressWhithIds(Long idCliente, Long idEndereco) {
		  Cliente cliente;

	        try {
	            cliente = clienteService.getById(idCliente);
	        } catch (Exception e) {
	            throw new RuntimeException("Não há cliente cadastrado com este id");
	        }

	        List<Endereco> enderecos = cliente.getEnderecos();
	        if (enderecos.isEmpty())
	            throw new RuntimeException("Não há endereços cadastrados para este cliente");

	        Optional<Endereco> endereco = enderecoRepository.findByIdAndCliente(idEndereco, cliente);
	        endereco.orElseThrow(() -> new RuntimeException("Não há endereço cadastrado com este id para este cliente"));

	        return endereco.get();
	}

	
	@Transactional
	public Page<Endereco> getAll(Pageable pageable) {
		Page<Endereco> findAll = enderecoRepository.findAll(pageable);
		return findAll;
	}


}
