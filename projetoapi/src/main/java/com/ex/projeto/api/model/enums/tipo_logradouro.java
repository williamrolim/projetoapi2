package com.ex.projeto.api.model.enums;

public enum tipo_logradouro {
	AEROPORTO ("aeroporto"),
	ALAMEDA ("alameda"),
	AREA("área"),
	AVENIDA("avenida"),
	CAMPO("campo"),
	CHACARA("chácara"),
	COLONIA("colônia"),
	CONDOMINIO("condomínio"),
	CONJUNTO("conjunto"),
	DISTRITO("distrito"),
	ESPLANADA("esplanada"),
	ESTACAO("estação"),
	ESTRADA("estrada"),
	FAVELA("favela"),
	FAZENDA("fazenda"),
	FEIRA("feira"),
	JARDIM("jardim"),
	LADEIRA("ladeira"),
	LAGO("lago"),
	LAGOA("lagoa"),
	LARGO ("largo"),
	LOTEAMENTO("loteamento"),
	MORRO("morro"),
	NUCLEO("núcleo"),
	PARQUE("parque"),
	PASSARELA("passarela"),
	PATIO("pátio"),
	PRACA("praça"),
	QUADRA("quadra"),
	RECANTO("recanto"),
	RESIDENCIAL("residencial"),
	RODOVIA("rodovia"),
	RUA("rua"),
	SETOR("setor"),
	SITIO("sítio"),
	TRAVESSA("travessa"),
	TRECHO("trecho"),
	TREVO("trevo"),
	VALE("vale"),
	VEREDA("vereda"),
	VIA("via"),
	VIADUTO("viaduto"),
	VIELA("viela"),
	VILA("vila");
	
	private final String logradouro;
	
	private tipo_logradouro(String logradouro) {
		this.logradouro = logradouro;
	}
	
	public String getLogradouros() {
		return logradouro;
	}
}
