package com.ex.projeto.api.service;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.ex.projeto.api.model.Cliente;
import com.ex.projeto.api.model.Documento;
import com.ex.projeto.api.model.Endereco;
import com.ex.projeto.api.model.enums.TipoDoc;
import com.ex.projeto.api.repository.ClienteRepository;
import com.ex.projeto.api.repository.DocumentoRepository;
@Service
public class DocumentoService {
	
	@Autowired
	private DocumentoRepository documentoRepository;
	
	@Autowired ClienteRepository clienteRepository;
	
	@Transactional
	public Documento saved(Documento documento) {
		Documento doc = documentoRepository.save(documento);
		return doc;
	}

	public Page<Documento> getAll(Pageable pageable) {
		Page<Documento> findAll = documentoRepository.findAll(pageable);
		return findAll;
	}

	public Documento getAllClientsAndDocumentsWhithIds(Long idCliente, Long idDocumento) {
		  Cliente cliente;

	        try {
	            cliente = clienteRepository.getById(idCliente);
	        } catch (Exception e) {
	            throw new RuntimeException("Não há cliente cadastrado com este id");
	        }

	        List<Documento> documentos = cliente.getDocumentos();
	        if (documentos.isEmpty())
	            throw new RuntimeException("Não existe documentos para o cliente solicitado");

	        Optional<Documento> documento = documentoRepository.findByIdAndCliente(idDocumento, cliente);
	        documento.orElseThrow(() -> new RuntimeException("Não existe documento cadastrado com este id para este cliente"));

	        return documento.get();
	}
	

}
